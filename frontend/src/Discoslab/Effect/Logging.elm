module Discoslab.Effect.Logging exposing
    ( Effect
    , Severity(..)
    , toCmd
    )

import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Http
import Json.Encode


type Severity
    = Debug
    | Info
    | Notice
    | Warning
    | Error
    | Critical
    | Alert
    | Emergency


weight : Severity -> Int
weight severity =
    case severity of
        Debug ->
            0

        Info ->
            1

        Notice ->
            2

        Warning ->
            3

        Error ->
            4

        Critical ->
            5

        Alert ->
            6

        Emergency ->
            7


type alias Effect =
    { severity : Severity
    , message : String
    , namespace : List String
    , context : Maybe Json.Encode.Value
    }


encodeLog : Effect -> Json.Encode.Value
encodeLog log =
    let
        severity =
            case log.severity of
                Debug ->
                    "debug"

                Info ->
                    "info"

                Notice ->
                    "notice"

                Warning ->
                    "warning"

                Error ->
                    "error"

                Critical ->
                    "critical"

                Alert ->
                    "alert"

                Emergency ->
                    "emergency"
    in
    Json.Encode.object
        [ ( "severity", Json.Encode.string severity )
        , ( "message", Json.Encode.string log.message )
        , ( "namespace", Json.Encode.list Json.Encode.string log.namespace )
        , ( "context", Maybe.withDefault Json.Encode.null log.context )
        ]


toCmd :
    { deps
        | noOp : msg
        , minSeverity : Severity
        , namespace : List String
        , api : BasePath
    }
    -> Effect
    -> Cmd msg
toCmd deps log =
    if weight log.severity >= weight deps.minSeverity then
        let
            body =
                { log | namespace = List.concat [ deps.namespace, log.namespace ] }
                    |> encodeLog
                    |> Http.jsonBody
        in
        Http.post
            { url = BasePath.absolute deps.api [ "alerts" ] []
            , expect = Http.expectWhatever (\_ -> deps.noOp)
            , body = body
            }

    else
        Cmd.none
