module Discoslab.Ui.Form exposing
    ( Config
    , config
    , setOnSubmit
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Json.Decode as Decode


type Config msg
    = Config
        { id : String
        , onSubmit : Maybe msg
        }


config : String -> Config msg
config id =
    Config
        { id = id
        , onSubmit = Nothing
        }


setOnSubmit : msg -> Config msg -> Config msg
setOnSubmit onSubmit (Config conf) =
    Config { conf | onSubmit = Just onSubmit }


view : Config msg -> List (Html msg) -> Html msg
view (Config conf) children =
    let
        attrs =
            [ Attrs.id conf.id
            , Attrs.class "form"
            ]

        events =
            case conf.onSubmit of
                Just onSubmit ->
                    [ Events.preventDefaultOn "submit" (Decode.succeed ( onSubmit, True )) ]

                Nothing ->
                    []
    in
    Html.form (attrs ++ events) children
