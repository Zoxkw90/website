module Discoslab.Ui.Layouts.Root exposing (view)

import Discoslab.Ui.Text.Link as Link
import Html exposing (Html)
import Html.Attributes as Attrs


view : List (Html msg) -> List (Html msg)
view children =
    [ Html.div [ Attrs.class "root" ]
        [ Html.header [ Attrs.class "root__header" ] []
        , Html.main_ [ Attrs.class "root__main" ] children
        , Html.footer [ Attrs.class "root__footer" ]
            [ Link.config "https://gitlab.com/discoslab"
                |> Link.setDisplayText "src"
                |> Link.view
            ]
        ]
    ]
