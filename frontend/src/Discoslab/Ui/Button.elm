module Discoslab.Ui.Button exposing
    ( Config
    , Relation(..)
    , Type(..)
    , config
    , setIsBusy
    , setIsDisabled
    , setOnClick
    , setRelation
    , setType
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events


type Type
    = Normal
    | Reset
    | Submit


type Relation
    = Primary
    | Secondary
    | Dangerous


type Config msg
    = Config
        { id : String
        , label : String
        , onClick : Maybe msg
        , type_ : Type
        , relation : Relation
        , isDisabled : Bool
        , isBusy : Bool
        }


config : { id : String, label : String } -> Config msg
config { id, label } =
    Config
        { id = id
        , label = label
        , onClick = Nothing
        , type_ = Normal
        , relation = Secondary
        , isDisabled = False
        , isBusy = False
        }


setOnClick : msg -> Config msg -> Config msg
setOnClick onClick (Config conf) =
    Config { conf | onClick = Just onClick }


setType : Type -> Config msg -> Config msg
setType type_ (Config conf) =
    Config { conf | type_ = type_ }


setRelation : Relation -> Config msg -> Config msg
setRelation relation (Config conf) =
    Config { conf | relation = relation }


setIsDisabled : Bool -> Config msg -> Config msg
setIsDisabled isDisabled (Config conf) =
    Config { conf | isDisabled = isDisabled }


setIsBusy : Bool -> Config msg -> Config msg
setIsBusy isBusy (Config conf) =
    Config { conf | isBusy = isBusy }


view : Config msg -> Html msg
view (Config conf) =
    let
        typeString =
            case conf.type_ of
                Normal ->
                    "button"

                Reset ->
                    "reset"

                Submit ->
                    "submit"

        ( isPrimary, isDangerous ) =
            case conf.relation of
                Primary ->
                    ( True, False )

                Dangerous ->
                    ( False, True )

                _ ->
                    ( False, False )

        attrs =
            List.filterMap identity
                [ Just <| Attrs.id conf.id
                , Just <| Attrs.type_ typeString
                , Just <| Attrs.class "button"
                , Just <|
                    Attrs.classList
                        [ ( "button--primary", isPrimary )
                        , ( "button--dangerous", isDangerous )
                        , ( "button--busy", conf.isBusy )
                        ]
                , Maybe.map (\onClick -> Events.onClick onClick) conf.onClick
                ]
    in
    Html.button attrs [ Html.text conf.label ]
