module Discoslab.Ui.Fields.TextArea exposing
    ( Config
    , config
    , setError
    , setHelp
    , setIsDisabled
    , setIsRequired
    , setMaxLength
    , setOnBlur
    , setOnInput
    , setValue
    , view
    )

import Discoslab.Views.Fields.Shared as Shared
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events


type Config msg
    = Config
        { id : String
        , label : String
        , value : String
        , onInput : Maybe (String -> msg)
        , onBlur : Maybe msg
        , maxLength : Maybe Int
        , isRequired : Bool
        , isDisabled : Bool
        , error : Maybe String
        , help : Maybe String
        }


config : { id : String, label : String } -> Config msg
config { id, label } =
    Config
        { id = id
        , label = label
        , value = ""
        , onInput = Nothing
        , onBlur = Nothing
        , maxLength = Nothing
        , isRequired = False
        , isDisabled = False
        , error = Nothing
        , help = Nothing
        }


setValue : String -> Config msg -> Config msg
setValue value (Config conf) =
    Config { conf | value = value }


setOnInput : (String -> msg) -> Config msg -> Config msg
setOnInput onInput (Config conf) =
    Config { conf | onInput = Just onInput }


setOnBlur : msg -> Config msg -> Config msg
setOnBlur onBlur (Config conf) =
    Config { conf | onBlur = Just onBlur }


setMaxLength : Maybe Int -> Config msg -> Config msg
setMaxLength maxLength (Config conf) =
    Config { conf | maxLength = maxLength }


setIsRequired : Bool -> Config msg -> Config msg
setIsRequired isRequired (Config conf) =
    Config { conf | isRequired = isRequired }


setIsDisabled : Bool -> Config msg -> Config msg
setIsDisabled isDisabled (Config conf) =
    Config { conf | isDisabled = isDisabled }


setError : Maybe String -> Config msg -> Config msg
setError error (Config conf) =
    Config { conf | error = error }


setHelp : Maybe String -> Config msg -> Config msg
setHelp help (Config conf) =
    Config { conf | help = help }


view : Config msg -> Html msg
view (Config conf) =
    let
        textareaAttrs =
            List.filterMap identity
                [ Just <| Attrs.class "field__input field__input--textarea"
                , Just <| Attrs.id conf.id
                , Just <| Attrs.value conf.value
                , Just <| Attrs.disabled conf.isDisabled
                , Maybe.map (\maxLength -> Attrs.maxlength maxLength) conf.maxLength
                , Maybe.map (\onInput -> Events.onInput onInput) conf.onInput
                , Maybe.map (\onBlur -> Events.onBlur onBlur) conf.onBlur
                ]

        input =
            Html.textarea textareaAttrs []

        sharedConf =
            { id = conf.id
            , label = conf.label
            , isRequired = conf.isRequired
            , isDisabled = conf.isDisabled
            , error = conf.error
            , help = conf.help
            }
    in
    Shared.view sharedConf input
