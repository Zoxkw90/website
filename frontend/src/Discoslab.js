import { Elm } from "./Main.elm";

const flags = {
  webapiBasePath: process.env.DISCOSLAB_WEBAPI_BASE_PATH,
  frontendBasePath: process.env.DISCOSLAB_FRONTEND_BASE_PATH,
  loggingMinSeverity: process.env.DISCOSLAB_FRONTEND_LOGGING_MIN_SEVERITY,
};

Elm.Main.init({ flags });
