module Main exposing (Model, Msg, main)

import Browser
import Browser.Navigation as Navigation
import Dict
import Discoslab
import Discoslab.Effect as Effect
import Discoslab.Effect.Logging as Logging
import Flags
import Html
import Json.Decode as Decode
import Json.Encode as Encode
import Url exposing (Url)


type Msg
    = NoOp
    | DiscoslabMsg Discoslab.Msg


type alias Model =
    { dependencies : Effect.Dependencies Msg
    , discoslab : Discoslab.Model
    }


init : Decode.Value -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init jsonFlags url navigationKey =
    let
        ( flags, flagsError ) =
            Flags.decode jsonFlags

        flagsErrorLog =
            if Dict.isEmpty flagsError then
                Effect.None

            else
                Effect.Logging
                    { severity = Logging.Alert
                    , message = "Failed to decode one or more flags."
                    , namespace = []
                    , context =
                        Dict.map (\_ -> Decode.errorToString) flagsError
                            |> Encode.dict identity Encode.string
                            |> Just
                    }

        dependencies =
            { api = flags.webapiBasePath
            , namespace = [ "frontend" ]
            , minSeverity = flags.minSeverity
            , noOp = NoOp
            , navigationKey = navigationKey
            }

        ( discoslab, discoslabEffect ) =
            Discoslab.init url

        cmd =
            Effect.Batch
                [ flagsErrorLog
                , Effect.map DiscoslabMsg discoslabEffect
                ]
                |> Effect.toCmd dependencies

        model =
            { dependencies = dependencies
            , discoslab = discoslab
            }
    in
    ( model, cmd )


subscriptions : Model -> Sub Msg
subscriptions model =
    Discoslab.subscriptions model.discoslab
        |> Sub.map DiscoslabMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        DiscoslabMsg discoslabMsg ->
            let
                ( updatedDiscoslab, effect ) =
                    Discoslab.update discoslabMsg model.discoslab
            in
            ( { model | discoslab = updatedDiscoslab }
            , Effect.map DiscoslabMsg effect
                |> Effect.toCmd model.dependencies
            )


view : Model -> Browser.Document Msg
view model =
    let
        { title, body } =
            Discoslab.view model.discoslab
    in
    { title = title
    , body = List.map (Html.map DiscoslabMsg) body
    }


main : Program Decode.Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = DiscoslabMsg << Discoslab.UrlRequested
        , onUrlChange = DiscoslabMsg << Discoslab.UrlChanged
        }
