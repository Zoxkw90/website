-- | Types and function for adjusting the min severity settings for logs.
module Discoslab.Webapi.Logging.MinSeverity
  ( MinSeverity (..)
  , def
  , toPermitFunc
  , toText
  , fromText
  , var
  )
where

import Data.Text
import Data.Text qualified as Text
import Discoslab.Webapi.Shared.BoundedEnum qualified as BoundedEnum
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified
import Katip qualified


-- | Min severity a log needs to be in order to be saved.
newtype MinSeverity = MinSeverity Katip.Severity
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Default min severity. Defaults to @MinSeverity Katip.DebugS@.
def :: MinSeverity
def =
  MinSeverity minBound


-- | Convert the min severity to a 'Katip.PermitFunc' that will
-- not permit any logs that are less than the 'MinSeverity'.
toPermitFunc :: MinSeverity -> Katip.PermitFunc
toPermitFunc (MinSeverity severity) =
  Katip.permitItem severity


-- | Convert the 'MinSeverity' to 'Text'.
toText :: MinSeverity -> Text
toText (MinSeverity severity) =
  case severity of
    Katip.DebugS -> "debug"
    Katip.InfoS -> "info"
    Katip.NoticeS -> "notice"
    Katip.WarningS -> "warning"
    Katip.ErrorS -> "error"
    Katip.AlertS -> "alert"
    Katip.CriticalS -> "critical"
    Katip.EmergencyS -> "emergency"


-- | Convert from 'Text' to a 'MinSeverity'. See 'toText' for valid values.
fromText :: Text -> Maybe MinSeverity
fromText =
  BoundedEnum.fromText toText


-- | Parse 'MinSeverity' from an environment variable.
var :: Env.Parser Env.Error MinSeverity
var =
  let validValues = Text.intercalate ", " $ fmap toText [minBound @MinSeverity ..]
   in Env.var (parseString fromText) "MIN_SEVERITY" . mconcat $
        [ Env.help . Text.unpack $
            "Min severity a log needs to be in order to be saved. Valid values from least to greatest: " <> validValues
        , Env.def def
        , Env.helpDef (Text.unpack . toText)
        ]
