-- | Middleware allows us to modify all requests/responses.
module Discoslab.Webapi.Http.Middleware
  ( ApplicationT
  , MiddlewareT
  , liftApplication
  , unliftApplication
  , liftMiddleware
  , apply
  )
where

import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.IO.Unlift (MonadUnliftIO (..), askRunInIO)
import Data.Coerce (coerce)
import Data.Monoid (Endo (..))
import Discoslab.Webapi.AppM (AppM)
import Discoslab.Webapi.Logging.Exceptions (withExceptionLogging)
import GHC.Stack (HasCallStack)
import Katip qualified
import Katip.Wai qualified
import Network.Wai qualified as Wai


-- | Version of 'Wai.Application' that supports running in a monad other than 'IO'.
type ApplicationT m =
  Wai.Request
  -> (Wai.Response -> m Wai.ResponseReceived)
  -> m Wai.ResponseReceived


-- | Version of 'Wai.Middleware' that supports running in a monad other than 'IO'.
type MiddlewareT m =
  ApplicationT m -> ApplicationT m


-- | Lift the 'IO' in 'Wai.Application' to 'AppM'.
liftApplication :: Wai.Application -> ApplicationT AppM
liftApplication application request send =
  withRunInIO $ \runInIO ->
    application request (runInIO . send)


-- | Unlift the 'AppM in 'Wai.ApplicationT' to 'IO'.
unliftApplication :: ApplicationT AppM -> AppM Wai.Application
unliftApplication application = do
  runInIO <- askRunInIO
  pure $ \request send ->
    runInIO $ application request (liftIO . send)


-- | Lift the IO in 'Wai.Middleware' to 'AppM'.
liftMiddleware :: Wai.Middleware -> MiddlewareT AppM
liftMiddleware middleware application request send = do
  unliftedApp <- unliftApplication application
  let modifiedApp = middleware unliftedApp
  liftApplication modifiedApp request send


-- | Log the incoming requests and outgoing responses.
logRequestsAndResponses :: MiddlewareT AppM
logRequestsAndResponses =
  Katip.Wai.middleware Katip.DebugS


-- | Log any unhandled exceptions.
logExceptions :: HasCallStack => MiddlewareT AppM
logExceptions application request send =
  withExceptionLogging Katip.CriticalS $
    application request send


-- | Apply the middleware to a WAI application.
apply :: HasCallStack => MiddlewareT AppM
apply =
  appEndo . mconcat . coerce $
    -- middleware runs in the same order they are declared in this list.
    [ logRequestsAndResponses
    , logExceptions
    ]
