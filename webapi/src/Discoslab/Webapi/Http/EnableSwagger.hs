-- | Flag for enabling/disabling swagger.
module Discoslab.Webapi.Http.EnableSwagger
  ( EnableSwagger (..)
  , def
  , fromBool
  , toBool
  , var
  )
where

import Data.Char qualified as Char
import Discoslab.Webapi.Shared.Env (bool)
import Env qualified


-- | Indicates if swagger should be enabled or not.
data EnableSwagger
  = NoSwagger
  | YesSwagger
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Defaults to no swagger.
def :: EnableSwagger
def =
  NoSwagger


-- | Convert a 'Bool' to 'EnableSwagger'.
fromBool :: Bool -> EnableSwagger
fromBool False = NoSwagger
fromBool True = YesSwagger


-- | Convert 'EnableSwagger' to a 'Bool'.
toBool :: EnableSwagger -> Bool
toBool = \case
  NoSwagger -> False
  YesSwagger -> True


-- | Parse a 'Bool' for enabling swagger from an environment variable.
var :: Env.Parser Env.Error EnableSwagger
var =
  Env.var (fmap fromBool . bool) "ENABLE_SWAGGER" . mconcat $
    [ Env.help "Enable swagger docs. If true, swagger ui will be available under /docs."
    , Env.def def
    , Env.helpDef (fmap Char.toLower . show . toBool)
    ]
