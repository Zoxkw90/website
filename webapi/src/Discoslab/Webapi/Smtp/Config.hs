-- | Record for configuring the SMTP connection.
module Discoslab.Webapi.Smtp.Config
  ( SmtpConfig (..)
  , vars
  )
where

import Control.Monad ((<=<))
import Discoslab.Webapi.Email.From (EmailFrom)
import Discoslab.Webapi.Email.From qualified as EmailFrom
import Discoslab.Webapi.Smtp.Login (Login)
import Discoslab.Webapi.Smtp.Login qualified as Login
import Discoslab.Webapi.Smtp.Security (Security)
import Discoslab.Webapi.Smtp.Security qualified as Security
import Env qualified
import Network.Socket (HostName, PortNumber)
import Numeric.Natural (Natural)


-- | Configuration for SMTP.
data SmtpConfig = SmtpConfig
  { hostName :: HostName
  , port :: PortNumber
  , security :: Security
  , login :: Maybe Login
  , fromAddress :: EmailFrom
  , queueSize :: Natural
  }
  deriving (Show, Eq)


-- | Parse the 'SmtpConfig' from environment variables.
vars :: Env.Parser Env.Error SmtpConfig
vars =
  let hostNameVar =
        Env.var (Env.str <=< Env.nonempty) "HOST" . mconcat $
          [ Env.help "The SMTP server's hostname."
          , Env.def "localhost"
          , Env.helpDef id
          ]
      portVar =
        Env.var Env.auto "PORT" . mconcat $
          [ Env.help "The SMTP server's port number."
          , Env.def 25
          , Env.helpDef show
          ]
      queueSize =
        Env.var Env.auto "QUEUE_SIZE" . mconcat $
          [ Env.help "Max amount of emails to have queued up at once. Used for a TBQueue bound."
          , Env.def 500
          , Env.helpDef show
          ]
   in Env.prefixed "SMTP_" $
        SmtpConfig
          <$> hostNameVar
          <*> portVar
          <*> Security.var
          <*> Env.optional Login.vars
          <*> EmailFrom.vars
          <*> queueSize
