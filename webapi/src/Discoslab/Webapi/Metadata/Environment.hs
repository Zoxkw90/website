-- | Type and functions for describing the environment the application is running in.
-- For example: dev, qa, or prod.
module Discoslab.Webapi.Metadata.Environment
  ( Environment
  , fromText
  , toText
  , toKatipEnvironment
  , def
  , var
  )
where

import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified
import Katip qualified


-- | Environment the application is running in.
-- For example: dev, qa, or prod.
newtype Environment = Environment Text
  deriving (Show, Eq)


-- | Convert from 'Text' to an 'Environment'. Any non-empty string will work.
fromText :: Text -> Maybe Environment
fromText text
  | Text.null strippedText = Nothing
  | otherwise = Just $ Environment strippedText
 where
  strippedText = Text.strip text


-- | Convert 'Environment' to 'Text'.
toText :: Environment -> Text
toText =
  coerce


-- | Convert 'Environment' to 'Katip.Environment'.
toKatipEnvironment :: Environment -> Katip.Environment
toKatipEnvironment =
  coerce


-- | Default 'Environment'. Set to "local".
def :: Environment
def =
  Environment "local"


-- | Parse 'Environment' from an environment variable.
var :: Env.Parser Env.Error Environment
var =
  Env.var (parseString fromText) "ENVIRONMENT" . mconcat $
    [ Env.help "Environment the application is running in. For example dev, qa, or prod. Any non-empty string is valid."
    , Env.def def
    , Env.helpDef (Text.unpack . toText)
    ]


instance Aeson.ToJSON Environment where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON Environment where
  parseJSON = Aeson.withText "Environment" $ \text ->
    case fromText text of
      Nothing -> fail "Environment may not be an empty string."
      Just value -> pure value
