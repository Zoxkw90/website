-- | Type and functions for marking when the applicaiton started.
module Discoslab.Webapi.Metadata.StartedAt
  ( StartedAt
  , now
  , toUTCTime
  )
where

import Control.Monad.IO.Class (MonadIO (..))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Time qualified as Time


-- | UTC time and date for when the application started.
newtype StartedAt = StartedAt Time.UTCTime
  deriving (Show, Eq)


-- | Create a 'StartedAt' that is right now.
-- Notice we don't expose the constructo of 'StartedAt', which means this
-- is the only way to construct a new 'StartedAt'.
now :: MonadIO m => m StartedAt
now =
  liftIO $ fmap StartedAt Time.getCurrentTime


-- | Convert a 'StartedAt' to 'Time.UTCTime'.
toUTCTime :: StartedAt -> Time.UTCTime
toUTCTime =
  coerce


instance Aeson.ToJSON StartedAt where
  toJSON = Aeson.toJSON . toUTCTime
  toEncoding = Aeson.toEncoding . toUTCTime


instance Aeson.FromJSON StartedAt where
  parseJSON =
    fmap StartedAt . Aeson.parseJSON
