-- | Request body for @POST ~/contacts@.
module Discoslab.Webapi.Contacts.Http.Post.Request
  ( FromRequest (..)
  , Request (..)
  , toEmail
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.Maybe (catMaybes)
import Data.OpenApi qualified as OpenApi
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Admin.Emails (AdminEmails)
import Discoslab.Webapi.Admin.Emails qualified as AdminEmails
import Discoslab.Webapi.Contacts.Http.Post.Errors (Errors (Errors), FromErrors (FromErrors))
import Discoslab.Webapi.Contacts.Http.Post.Errors qualified as Errors
import Discoslab.Webapi.Contacts.Message (Message)
import Discoslab.Webapi.Contacts.Message qualified as Message
import Discoslab.Webapi.Contacts.Subject (Subject)
import Discoslab.Webapi.Contacts.Subject qualified as Subject
import Discoslab.Webapi.Email (Email (Email))
import Discoslab.Webapi.Email qualified as Email
import Discoslab.Webapi.Email.Address (EmailAddress)
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.From (EmailFrom)
import Discoslab.Webapi.Email.Name (EmailName)
import Discoslab.Webapi.Email.Name qualified as EmailName
import Discoslab.Webapi.Shared.OpenApi (DocumentedBy)
import Discoslab.Webapi.Shared.OpenApi qualified as SharedOpenApi
import Env.Generic (Generic)


-- | Optionally provide a name or email so we can respond to the author.
data FromRequest = FromRequest
  { name :: Maybe (DocumentedBy EmailName Text)
  , email :: Maybe (DocumentedBy EmailAddress Text)
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON FromRequest
instance Aeson.FromJSON FromRequest


instance OpenApi.ToSchema FromRequest where
  declareNamedSchema proxy = do
    namedSchema <- OpenApi.genericDeclareNamedSchema OpenApi.defaultSchemaOptions proxy
    pure $
      namedSchema
        & OpenApi.name ?~ "ContactsPostFromRequest"
        & OpenApi.schema . OpenApi.description ?~ "Optionally provide a name or email so we can respond to the author."


-- | Request body shape for @POST ~/contacts@. Represents a message someone wants to send us.
data Request = Request
  { from :: Maybe FromRequest
  , subject :: Maybe (DocumentedBy Subject Text)
  , message :: DocumentedBy Message Text
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Request
instance Aeson.FromJSON Request


instance OpenApi.ToSchema Request where
  declareNamedSchema proxy = do
    namedSchema <- OpenApi.genericDeclareNamedSchema OpenApi.defaultSchemaOptions proxy
    pure $
      namedSchema
        & OpenApi.name ?~ "ContactsPostRequest"
        & OpenApi.schema . OpenApi.description ?~ "Message to send to the website's admin."


-- | Convert the request body to an email destined for the website admins.
toEmail :: EmailFrom -> AdminEmails -> Request -> Either Errors Email
toEmail from adminEmails request =
  let nameResult =
        let maybeText = request.from >>= (.name) <&> (.unwrap)
         in traverse EmailName.fromText maybeText
      emailResult =
        let maybeText = request.from >>= (.email) <&> (.unwrap)
         in traverse EmailAddress.fromText maybeText
      subjectResult =
        let maybeText = request.subject <&> (.unwrap)
         in traverse Subject.fromText maybeText
      messageResult =
        let text = request.message.unwrap
         in Message.fromText text
      fields =
        ( nameResult
        , emailResult
        , subjectResult
        , messageResult
        )
   in case fields of
        ( Right name
          , Right email
          , Right subject
          , Right message
          ) ->
            Right $
              Email
                { from
                , to = coerce adminEmails
                , cc = []
                , bcc = []
                , subject = Just "Message sent through Disco's Lab"
                , body =
                    Text.strip . Text.unlines . catMaybes $
                      [ ("Subject: " <>) . Subject.toText <$> subject
                      , ("Name: " <>) . EmailName.toText <$> name
                      , ("Email: " <>) . EmailAddress.toText <$> email
                      , Just ""
                      , Just $ Message.toText message
                      ]
                }
        _ ->
          Left
            Errors
              { from =
                  FromErrors
                    { name = either Just (const Nothing) nameResult
                    , email = either Just (const Nothing) emailResult
                    }
              , subject = either Just (const Nothing) subjectResult
              , message = either Just (const Nothing) messageResult
              }
