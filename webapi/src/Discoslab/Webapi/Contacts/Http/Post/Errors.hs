-- | Error response shapes for the @POST ~/contacts@.
module Discoslab.Webapi.Contacts.Http.Post.Errors
  ( FromErrors (..)
  , Errors (..)
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Discoslab.Webapi.Contacts.Message qualified as Message
import Discoslab.Webapi.Contacts.Subject qualified as Subject
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.Name qualified as EmailName
import Env.Generic (Generic)


-- | Errors describing why the @from@ fields were rejected.
data FromErrors = FromErrors
  { name :: Maybe EmailName.Error
  , email :: Maybe EmailAddress.Error
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON FromErrors
instance Aeson.FromJSON FromErrors


instance OpenApi.ToSchema FromErrors where
  declareNamedSchema proxy = do
    namedSchema <- OpenApi.genericDeclareNamedSchema OpenApi.defaultSchemaOptions proxy
    pure $
      namedSchema
        & OpenApi.name ?~ "ContactsPostFromErrors"
        & OpenApi.schema . OpenApi.description ?~ "Error messages describing why the ContactsPostFromRequest was bad."


-- | Errors describing why the JSON request body sent to @POST ~/contacts@ was rejected.
data Errors = Errors
  { from :: FromErrors
  , subject :: Maybe Subject.Error
  , message :: Maybe Message.Error
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Errors
instance Aeson.FromJSON Errors


instance OpenApi.ToSchema Errors where
  declareNamedSchema proxy = do
    namedSchema <- OpenApi.genericDeclareNamedSchema OpenApi.defaultSchemaOptions proxy
    pure $
      namedSchema
        & OpenApi.name ?~ "ContactsPostErrors"
        & OpenApi.schema . OpenApi.description ?~ "Error messages describing why the ContactsPostRequest was bad."
