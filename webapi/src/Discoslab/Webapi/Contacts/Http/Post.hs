-- | Handler for @POST ~/contacts@.
module Discoslab.Webapi.Contacts.Http.Post
  ( Response
  , Api
  , server
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad.Reader (asks)
import Data.Has (Has (..))
import Data.Text (Text)
import Discoslab.Webapi.AppM (AppM)
import Discoslab.Webapi.Contacts.Http.Post.Errors (Errors)
import Discoslab.Webapi.Contacts.Http.Post.Request (Request)
import Discoslab.Webapi.Contacts.Http.Post.Request qualified as Request
import Discoslab.Webapi.Email qualified as Email
import Servant
  ( Description
  , HasServer (ServerT)
  , JSON
  , ReqBody
  , StdMethod (POST)
  , Summary
  , UVerb
  , WithStatus (..)
  , respond
  , type (:>)
  )


type Response =
  '[ WithStatus 202 Text
   , WithStatus 400 Errors
   ]


type Api =
  Summary "Send a message to the website's admin."
    :> Description "Provides a way to communicate with me without sharing my email address."
    :> ReqBody '[JSON] Request
    :> UVerb 'POST '[JSON] Response


server :: ServerT Api AppM
server request =
  checkpointCallStack $ do
    from <- asks getter
    adminEmails <- asks getter

    case Request.toEmail from adminEmails request of
      Left errors ->
        respond $ WithStatus @400 errors
      Right email -> do
        Email.send email
        respond $ WithStatus @202 @Text "Message sent."
