-- | Type and functions for dealing with the body of an email message.
module Discoslab.Webapi.Contacts.Message
  ( Message
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Shared.BoundedEnum qualified as BoundedEnum


-- | Body of an email message.
newtype Message = Message Text
  deriving (Show, Eq)


-- | Error describing why an email message was rejected.
data Error
  = Empty
  | LongerThan1000Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert the 'Error' to a human suitable 'Text'.
errorText :: Error -> Text
errorText = \case
  Empty -> "Message is empty."
  LongerThan1000Characters -> "Message is longer than 1000 characters."


-- | Convert from 'Text' to an 'Message'.
-- See 'Error' for reasons why this may return the 'Left' variant.
fromText :: Text -> Either Error Message
fromText text
  | Text.null strippedText = Left Empty
  | Text.length strippedText > 1000 = Left LongerThan1000Characters
  | otherwise = Right $ Message strippedText
 where
  strippedText = Text.strip text


-- | Convert an 'Message' to 'Text'.
toText :: Message -> Text
toText =
  coerce


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance Aeson.FromJSON Error where
  parseJSON =
    BoundedEnum.parseJSON errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "ContactsMessageError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Error message describing why a message was rejected."
        & OpenApi.example ?~ Aeson.toJSON Empty
        & OpenApi.enum_ ?~ fmap (Aeson.toJSON @Error) [minBound ..]


instance Aeson.ToJSON Message where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON Message where
  parseJSON = Aeson.withText "ContactsMessage" $ \text ->
    case fromText text of
      Left err -> fail . Text.unpack $ errorText err
      Right value -> pure value


instance OpenApi.ToSchema Message where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "ContactsMessage") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Body of an email message."
        & OpenApi.example ?~ Aeson.toJSON @Text "This is an example message."
