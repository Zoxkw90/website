-- | Metadata describing things about this instance.
module Discoslab.Webapi.Metadata
  ( Metadata (..)
  , def
  , vars
  )
where

import Data.Aeson qualified as Aeson
import Discoslab.Webapi.Metadata.Environment (Environment)
import Discoslab.Webapi.Metadata.Environment qualified as Environment
import Discoslab.Webapi.Metadata.InstanceId (InstanceId)
import Discoslab.Webapi.Metadata.StartedAt (StartedAt)
import Discoslab.Webapi.Metadata.Version (Version)
import Discoslab.Webapi.Metadata.Version qualified as Version
import Env qualified
import GHC.Generics (Generic)


-- | Metadata describing things about this instance.
data Metadata = Metadata
  { environment :: Environment
  , instanceId :: InstanceId
  , version :: Version
  , startedAt :: StartedAt
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Metadata
instance Aeson.FromJSON Metadata


-- | Default 'Metadata'. Calls @def@ from the respective modules.
def :: InstanceId -> StartedAt -> Metadata
def instanceId startedAt =
  Metadata
    { instanceId
    , startedAt
    , environment = Environment.def
    , version = Version.def
    }


-- | Parse the 'Metadata' from environment variables.
vars :: InstanceId -> StartedAt -> Env.Parser Env.Error Metadata
vars instanceId startedAt =
  Metadata
    <$> Environment.var
    <*> pure instanceId
    <*> Version.var
    <*> pure startedAt
