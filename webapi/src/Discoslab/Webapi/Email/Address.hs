-- | Type and functions for email addresses.
module Discoslab.Webapi.Email.Address
  ( EmailAddress
  , Error (..)
  , errorText
  , fromText
  , toText
  , domainPart
  , localPart
  , toMimeAddress
  , var
  , webapiAtDiscoslabDotCom
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Encoding (decodeUtf8Lenient, encodeUtf8)
import Discoslab.Webapi.Email.Name (EmailName)
import Discoslab.Webapi.Email.Name qualified as EmailName
import Discoslab.Webapi.Shared.BoundedEnum qualified as BoundedEnum
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified
import Network.Mail.Mime qualified as Mime
import Text.Email.Validate (validate)
import Text.Email.Validate qualified as Validate


-- | An email address to send mail to or from. Such as "john.smith@example.com".
newtype EmailAddress = EmailAddress Validate.EmailAddress
  deriving (Show, Eq, Ord)


-- | Error describing why an 'Email' could not be contstructed.
data Error
  = Invalid
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert an 'Error' to a human suitable 'Text'.
errorText :: Error -> Text
errorText = \case
  Invalid -> "Email address is invalid."


-- | Convert from 'Text' to an 'EmailAddress'.
-- The 'Text' for the 'Left' variant comes from 'validate'.
fromText :: Text -> Either Error EmailAddress
fromText text =
  case validate (encodeUtf8 $ Text.strip text) of
    Left _ -> Left Invalid
    Right validated -> Right $ EmailAddress validated


-- | Convert an 'EmailAddress' to a 'Text'.
toText :: EmailAddress -> Text
toText (EmailAddress validated) =
  decodeUtf8Lenient $ Validate.toByteString validated


-- | Get the domain part of an email. For example "john.smith@example.com" would return "example.com".
domainPart :: EmailAddress -> Text
domainPart (EmailAddress validated) =
  decodeUtf8Lenient $ Validate.domainPart validated


-- | Get the local part of an email. For example "john.smith@example.com" would return "john.smith".
localPart :: EmailAddress -> Text
localPart (EmailAddress validated) =
  decodeUtf8Lenient $ Validate.localPart validated


-- | Convert to a 'Mime.Address'.
toMimeAddress :: Maybe EmailName -> EmailAddress -> Mime.Address
toMimeAddress addressName emailAddress =
  Mime.Address
    { Mime.addressName = fmap EmailName.toText addressName
    , Mime.addressEmail = toText emailAddress
    }


-- | webapi@discoslab.com
webapiAtDiscoslabDotCom :: EmailAddress
webapiAtDiscoslabDotCom =
  EmailAddress $ Validate.unsafeEmailAddress "webapi" "discoslab.com"


-- | Parse 'EmailAddress' from an environment variable.
var :: Env.Parser Env.Error EmailAddress
var =
  let emailAddress =
        parseString $ \text ->
          case fromText text of
            Left _ -> Nothing
            Right value -> Just value
   in Env.var emailAddress "EMAIL_ADDRESS" . mconcat $
        [ Env.help . Text.unpack $
            "The email address to send emails from."
        , Env.def webapiAtDiscoslabDotCom
        , Env.helpDef (Text.unpack . toText)
        ]


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance Aeson.FromJSON Error where
  parseJSON =
    BoundedEnum.parseJSON errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "EmailAddressError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Error message describing why an email address was rejected."
        & OpenApi.example ?~ Aeson.toJSON (minBound @Error)
        & OpenApi.enum_ ?~ fmap (Aeson.toJSON @Error) [minBound ..]


instance Aeson.ToJSON EmailAddress where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON EmailAddress where
  parseJSON = Aeson.withText "EmailAddress" $ \text ->
    case fromText text of
      Left err -> fail . Text.unpack $ errorText err
      Right email -> pure email


instance OpenApi.ToSchema EmailAddress where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "EmailAddress") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "An email address identifies an email box to which messages are delivered."
        & OpenApi.example ?~ Aeson.toJSON @Text "john.smith@example.com"
