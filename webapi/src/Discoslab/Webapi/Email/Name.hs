-- | Type and functions for dealing with the name associated to an email.
module Discoslab.Webapi.Email.Name
  ( EmailName
  , Error (..)
  , errorText
  , fromText
  , toText
  , var
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Shared.BoundedEnum qualified as BoundedEnum
import Discoslab.Webapi.Shared.Env (parseString)
import Env qualified


-- | Name that is associated to an email. Usually a first and last name, such as "John Smith".
newtype EmailName = EmailName Text
  deriving (Show, Eq)


-- | An error describing why an 'EmailName' could not be constructed.
data Error
  = Empty
  | LongerThan100Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert the 'Error' to a 'Text' that is suitable for humans. 👽
errorText :: Error -> Text
errorText = \case
  Empty -> "Address name is empty."
  LongerThan100Characters -> "Address name is longer than 100 characters."


-- | Convert an unvalidated 'Text' to an 'EmailName'.
-- See the 'Error' type for reasons why this may return a 'Left' variant.
fromText :: Text -> Either Error EmailName
fromText text
  | Text.null strippedText = Left Empty
  | Text.length strippedText > 100 = Left LongerThan100Characters
  | otherwise = Right $ EmailName strippedText
 where
  strippedText = Text.strip text


-- | Convert the 'EmailName' to 'Text'.
toText :: EmailName -> Text
toText =
  coerce


-- | Parse 'EmailName' from an environment variable.
var :: Env.Parser Env.Error EmailName
var =
  let emailAddressName =
        parseString $ \text ->
          case fromText text of
            Left _ -> Nothing
            Right value -> Just value
   in Env.var emailAddressName "EMAIL_NAME" . mconcat $
        [ Env.help . Text.unpack $
            "The name associated to the from address when sending emails."
        , Env.def (EmailName "Disco's Lab")
        , Env.helpDef (Text.unpack . toText)
        ]


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance Aeson.FromJSON Error where
  parseJSON =
    BoundedEnum.parseJSON errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "EmailNameError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Error message describing why an email address name was rejected."
        & OpenApi.example ?~ Aeson.toJSON Empty
        & OpenApi.enum_ ?~ fmap (Aeson.toJSON @Error) [minBound ..]


instance Aeson.ToJSON EmailName where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON EmailName where
  parseJSON = Aeson.withText "EmailName" $ \text ->
    case fromText text of
      Left err -> fail . Text.unpack $ errorText err
      Right value -> pure value


instance OpenApi.ToSchema EmailName where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "EmailName") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Human readable name to go along with an email address. Usually a first and last name."
        & OpenApi.example ?~ Aeson.toJSON @Text "John Smith"
