-- | Types and functions for globally accessed data.
module Discoslab.Webapi.AppData
  ( AppData (..)
  , with
  )
where

import Control.Monad.Cont (ContT (..))
import Control.Monad.IO.Unlift (MonadUnliftIO (..))
import Control.Monad.Trans (MonadTrans (..))
import Data.Has (Has (..))
import Discoslab.Webapi.Admin.Emails (AdminEmails)
import Discoslab.Webapi.Config (Config)
import Discoslab.Webapi.Config qualified as Config
import Discoslab.Webapi.Email (SendEmail (..))
import Discoslab.Webapi.Email.From (EmailFrom)
import Discoslab.Webapi.Logging.Data as LoggingData
import Discoslab.Webapi.Metadata (Metadata)
import Discoslab.Webapi.Smtp.Config qualified as SmtpConfig
import Discoslab.Webapi.Smtp.Thread qualified as SmtpThread
import GHC.Stack (HasCallStack)


-- | Data that is accessible anywhere within the @AppM@ monad.
data AppData = AppData
  { metadata :: Metadata
  -- ^ Metadata about this application.
  , logging :: LoggingData
  -- ^ Data needed for logging.
  , fromAddress :: EmailFrom
  -- ^ Email address to send messages from.
  , sendEmail :: SendEmail
  -- ^ Port for sending emails.
  , adminEmails :: AdminEmails
  -- ^ Email addresses of website admins, so we can send alerts and whatnots.
  }


instance Has Metadata AppData where
  getter = (.metadata)
  modifier f appData = appData{metadata = f appData.metadata}


instance Has LoggingData AppData where
  getter = (.logging)
  modifier f appData = appData{logging = f appData.logging}


instance Has EmailFrom AppData where
  getter = (.fromAddress)
  modifier f appData = appData{fromAddress = f appData.fromAddress}


instance Has SendEmail AppData where
  getter = (.sendEmail)
  modifier f appData = appData{sendEmail = f appData.sendEmail}


instance Has AdminEmails AppData where
  getter = (.adminEmails)
  modifier f appData = appData{adminEmails = f appData.adminEmails}


-- | Construct the 'AppData' in a callback fashion to ensure all of the cleanup
-- code runs, even in the case of unhandled exceptions.
with :: (HasCallStack, MonadUnliftIO m) => Config -> (AppData -> m a) -> m a
with config =
  runContT $ do
    logging <- ContT $ LoggingData.withStdout config.metadata config.logging

    -- Allow the callback passed to 'ContT' to log through 'Katip'.
    let allowLogging resource =
          ContT $ \use ->
            LoggingData.runKatip logging . resource $ lift . use

    -- Emails will be sent asynchronously via SMTP.
    sendEmail <- do
      smtpThread <- allowLogging $ SmtpThread.with config.smtp
      pure . SendEmail $ SmtpThread.send smtpThread

    pure $
      AppData
        { metadata = config.metadata
        , fromAddress = config.smtp.fromAddress
        , adminEmails = config.adminEmails
        , ..
        }
