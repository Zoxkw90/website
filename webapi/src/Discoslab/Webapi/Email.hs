-- | Record and functions for email messages.
module Discoslab.Webapi.Email
  ( Email (..)
  , toMimeMail
  , SendEmail (..)
  , send
  )
where

import Control.Lens ((?~))
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader (MonadReader, asks)
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.Has (Has (getter))
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (catMaybes)
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text.Lazy qualified as LazyText
import Discoslab.Webapi.Email.Address (EmailAddress)
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.From (EmailFrom)
import Discoslab.Webapi.Email.From qualified as EmailFrom
import Discoslab.Webapi.Logging.Data (LoggingData)
import Discoslab.Webapi.Logging.Data qualified as LoggingData
import GHC.Generics (Generic)
import Katip qualified
import Network.Mail.Mime qualified as Mime


-- | A complete email message.
data Email = Email
  { from :: EmailFrom
  , to :: NonEmpty EmailAddress
  , cc :: [EmailAddress]
  , bcc :: [EmailAddress]
  , subject :: Maybe Text
  , body :: Text
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Email
instance Aeson.FromJSON Email


instance OpenApi.ToSchema Email where
  declareNamedSchema _ = do
    namedSchema <-
      OpenApi.genericDeclareNamedSchema @Email
        OpenApi.defaultSchemaOptions
        Proxy

    pure $
      namedSchema
        & OpenApi.name ?~ "EmailMessage"
        & OpenApi.schema . OpenApi.description ?~ "A message to be sent as an email."


-- | Convert the 'Email' to a 'Mime.Mail'.
toMimeMail :: Email -> Mime.Mail
toMimeMail email =
  let emptyMail =
        Mime.emptyMail $
          EmailAddress.toMimeAddress email.from.name email.from.address
      subjectHeader =
        fmap ("Subject",) email.subject
      plain =
        [ Mime.plainPart . LazyText.fromStrict $ email.body
        ]
   in emptyMail
        { Mime.mailTo =
            NonEmpty.toList $ fmap (EmailAddress.toMimeAddress Nothing) email.to
        , Mime.mailCc =
            fmap (EmailAddress.toMimeAddress Nothing) email.cc
        , Mime.mailBcc =
            fmap (EmailAddress.toMimeAddress Nothing) email.bcc
        , Mime.mailHeaders =
            catMaybes
              [ subjectHeader
              ]
        , Mime.mailParts =
            [ plain
            ]
        }


-- | A port for sending emails.
newtype SendEmail = SendEmail (Email -> Katip.KatipContextT IO ())


-- | Send an email.
-- __WARNING__ The behavior of this depends on 'SendEmail' in @r@.
send :: (MonadReader r m, Has SendEmail r, Has LoggingData r, MonadIO m) => Email -> m ()
send email = do
  SendEmail sendEmail <- asks getter
  LoggingData.liftKatipIO $ sendEmail email
