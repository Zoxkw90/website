-- | Stubs for 'SendEmail'.
module Discoslab.Webapi.Stubs.Email
  ( discardSentEmail
  , SentEmailsVar
  , saveSentEmailInMemory
  , getAllSentEmails
  )
where

import Control.Concurrent.STM qualified as STM
import Control.Concurrent.STM.TVar (TVar)
import Control.Concurrent.STM.TVar qualified as TVar
import Control.Monad.IO.Class (MonadIO (..))
import Data.Foldable (Foldable (toList))
import Data.Sequence (Seq, (|>))
import Data.Sequence qualified as Seq
import Discoslab.Webapi.Email (Email, SendEmail (SendEmail))


-- | Discard sent emails. Do nothing.
discardSentEmail :: SendEmail
discardSentEmail =
  SendEmail $ \_ -> pure ()


-- | All emails that have been sent.
newtype SentEmailsVar = SentEmailsVar (TVar (Seq Email))


-- | Save sent emails in memory.
-- You can use 'getAllSentEmails' to retrieve all emails that were sent.
saveSentEmailInMemory :: IO (SendEmail, SentEmailsVar)
saveSentEmailInMemory = do
  var <- TVar.newTVarIO Seq.empty

  let sendEmail =
        SendEmail $ \email ->
          liftIO . STM.atomically $ TVar.modifyTVar' var $! (|> email)

  pure (sendEmail, SentEmailsVar var)


-- | Get all emails that were sent.
getAllSentEmails :: SentEmailsVar -> IO [Email]
getAllSentEmails (SentEmailsVar var) =
  fmap toList . STM.atomically $ STM.readTVar var
