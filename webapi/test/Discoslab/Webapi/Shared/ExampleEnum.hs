module Discoslab.Webapi.Shared.ExampleEnum
  ( ExampleEnum (..)
  , exampleToText
  , exampleFromText
  , validExampleInputs
  , invalidExampleInputs
  )
where

import Data.Aeson qualified as Aeson
import Data.Text (Text)
import Discoslab.Webapi.Shared.BoundedEnum qualified as BoundedEnum


data ExampleEnum
  = FirstExample
  | SecondExample
  | ThirdExample
  deriving (Show, Eq, Bounded, Enum)


exampleToText :: ExampleEnum -> Text
exampleToText = \case
  FirstExample -> "first"
  SecondExample -> "second"
  ThirdExample -> "third"


exampleFromText :: Text -> Maybe ExampleEnum
exampleFromText =
  BoundedEnum.fromText exampleToText


validExampleInputs :: [(Text, ExampleEnum)]
validExampleInputs =
  [ ("first", FirstExample)
  , ("second", SecondExample)
  , ("third", ThirdExample)
  , ("FIRST", FirstExample)
  , ("SECOND", SecondExample)
  , ("THIRD", ThirdExample)
  , ("FiRsT", FirstExample)
  , ("SeCoND", SecondExample)
  , ("ThIrD", ThirdExample)
  , ("  first ", FirstExample)
  , ("  second ", SecondExample)
  , ("  third ", ThirdExample)
  , ("  FIRST   ", FirstExample)
  , ("  SECOND ", SecondExample)
  , ("  THIRD   ", ThirdExample)
  , ("  FiRsT ", FirstExample)
  , ("  SeCoND ", SecondExample)
  , ("  ThIrD   ", ThirdExample)
  ]


invalidExampleInputs :: [Text]
invalidExampleInputs =
  [ "frst"
  , ""
  , "   "
  , "sEc0nd"
  , "thirrd"
  , "dog"
  , "cat"
  ]


instance Aeson.ToJSON ExampleEnum where
  toJSON = Aeson.toJSON . exampleToText
  toEncoding = Aeson.toEncoding . exampleToText


instance Aeson.FromJSON ExampleEnum where
  parseJSON =
    BoundedEnum.parseJSON exampleToText
