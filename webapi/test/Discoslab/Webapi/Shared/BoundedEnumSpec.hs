module Discoslab.Webapi.Shared.BoundedEnumSpec
  ( spec
  )
where

import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as AesonTypes
import Data.Foldable (for_)
import Discoslab.Webapi.Shared.ExampleEnum
  ( ExampleEnum (..)
  , exampleFromText
  , invalidExampleInputs
  , validExampleInputs
  )
import Test.Hspec (Spec, describe, it, shouldBe)


exampleFromJson :: Aeson.Value -> Maybe ExampleEnum
exampleFromJson =
  AesonTypes.parseMaybe Aeson.parseJSON


spec :: Spec
spec = do
  describe "fromText" $ do
    describe "returns Just ExampleEnum for valid inputs" $
      for_ validExampleInputs $ \(input, expected) ->
        it (show input) $
          exampleFromText input `shouldBe` Just expected

    describe "returns Nothing for invalid inputs" $
      for_ invalidExampleInputs $ \input ->
        it (show input) $
          exampleFromText input `shouldBe` Nothing

  describe "parseJSON" $ do
    describe "returns Just ExampleEnum for valid inputs" $
      for_ validExampleInputs $ \(input, expected) ->
        it (show input) $
          exampleFromJson (Aeson.toJSON input) `shouldBe` Just expected

    describe "returns Nothing for invalid inputs" $
      for_ invalidExampleInputs $ \input ->
        it (show input) $
          exampleFromJson (Aeson.toJSON input) `shouldBe` Nothing
