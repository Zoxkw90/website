module Discoslab.Webapi.Contacts.HttpSpec (spec) where

import Data.Aeson ((.=))
import Data.Aeson qualified as Aeson
import Data.ByteString.Lazy qualified as LazyBs
import Data.ByteString.Lazy.Char8 qualified as LazyChar8
import Data.Coerce (coerce)
import Data.Foldable (for_)
import Data.Text (Text)
import Data.Text qualified as Text
import Discoslab.Webapi.Admin.Emails qualified as AdminEmails
import Discoslab.Webapi.AppData qualified as AppData
import Discoslab.Webapi.Email qualified as Email
import Discoslab.Webapi.Stubs.Email (SentEmailsVar)
import Discoslab.Webapi.Stubs.Email qualified as EmailStubs
import Discoslab.Webapi.TestApp (TestApp)
import Discoslab.Webapi.TestApp qualified as TestApp
import Network.HTTP.Client qualified as Http
import Network.HTTP.Types qualified as HttpTypes
import Test.Hspec (Spec, around, describe, it, runIO, shouldBe)


withTestApp :: ((TestApp, SentEmailsVar) -> IO a) -> IO a
withTestApp use = do
  initialAppData <- TestApp.stubbedData

  -- Override the 'SendEmail' stub so we can observe what emails were sent.

  (sendEmail, sentEmailsVar) <- EmailStubs.saveSentEmailInMemory

  let appData =
        initialAppData
          { AppData.sendEmail = sendEmail
          }

  TestApp.with appData $ \testApp ->
    use (testApp, sentEmailsVar)


post :: Http.Manager -> TestApp -> Aeson.Value -> IO (Http.Response LazyBs.ByteString)
post manager testApp requestBody = do
  initialRequest <- Http.parseRequest $ "http://localhost:" <> show testApp.httpPort <> "/contacts"

  let request =
        initialRequest
          { Http.method = HttpTypes.methodPost
          , Http.requestHeaders = [("Content-Type", "application/json")]
          , Http.requestBody = Http.RequestBodyBS . LazyBs.toStrict $ Aeson.encode requestBody
          }

  Http.httpLbs request manager


spec :: Spec
spec = do
  manager <- runIO $ Http.newManager Http.defaultManagerSettings

  around withTestApp . describe "POST ~/contacts" $ do
    describe "returns 400 for bad request bodies and does not send any email" $ do
      let examples =
            [
              ( Aeson.Null
              , Left "Error in $: parsing Discoslab.Webapi.Contacts.Http.Post.Request.Request(Request) failed, expected Object, but encountered Null"
              )
            ,
              ( Aeson.object
                  [ "message" .= Aeson.Null
                  ]
              , Left "Error in $.message: parsing Text failed, expected String, but encountered Null"
              )
            ,
              ( Aeson.object
                  [ "message" .= ("" :: Text)
                  ]
              , Right $
                  Aeson.object
                    [ "from"
                        .= Aeson.object
                          [ "name" .= Aeson.Null
                          , "email" .= Aeson.Null
                          ]
                    , "subject" .= Aeson.Null
                    , "message" .= ("Message is empty." :: Text)
                    ]
              )
            ,
              ( Aeson.object
                  [ "message" .= ("   " :: Text)
                  ]
              , Right $
                  Aeson.object
                    [ "from"
                        .= Aeson.object
                          [ "name" .= Aeson.Null
                          , "email" .= Aeson.Null
                          ]
                    , "subject" .= Aeson.Null
                    , "message" .= ("Message is empty." :: Text)
                    ]
              )
            ,
              ( Aeson.object
                  [ "message" .= Text.replicate 1001 "x"
                  , "from"
                      .= Aeson.object
                        [ "name" .= ("bob" :: Text)
                        , "email" .= ("not_a_valid_email_address_at_aol_dot_com" :: Text)
                        ]
                  ]
              , Right $
                  Aeson.object
                    [ "from"
                        .= Aeson.object
                          [ "name" .= Aeson.Null
                          , "email" .= ("Email address is invalid." :: Text)
                          ]
                    , "subject" .= Aeson.Null
                    , "message" .= ("Message is longer than 1000 characters." :: Text)
                    ]
              )
            ]

      for_ examples $ \(example, expectedResponseBody) -> do
        let title = LazyChar8.unpack $ Aeson.encode example

        it title $ \(testApp, sentEmailsVar) -> do
          response <- post manager testApp example
          emails <- EmailStubs.getAllSentEmails sentEmailsVar

          length emails `shouldBe` 0

          Http.responseStatus response `shouldBe` HttpTypes.status400

          case expectedResponseBody of
            Left raw -> Http.responseBody response `shouldBe` raw
            Right json -> do
              Just actualResponseJson <- pure $ Aeson.decode @Aeson.Value (Http.responseBody response)
              actualResponseJson `shouldBe` json

    describe "returns 202 for good request bodies and does send an email" $ do
      let examples =
            [
              ( Aeson.object
                  [ "message" .= ("This is an example message from a test." :: Text)
                  ]
              , "This is an example message from a test."
              )
            ,
              ( Aeson.object
                  [ "message" .= ("This is another example message from a test." :: Text)
                  , "from" .= Aeson.Null
                  ]
              , "This is another example message from a test."
              )
            ,
              ( Aeson.object
                  [ "message" .= ("This is another example message from a test." :: Text)
                  , "from"
                      .= Aeson.object
                        [ "name" .= ("Mr. Bob Dobalina" :: Text)
                        ]
                  ]
              , "Name: Mr. Bob Dobalina\n\nThis is another example message from a test."
              )
            ,
              ( Aeson.object
                  [ "message" .= ("This is another example message from a test." :: Text)
                  , "from"
                      .= Aeson.object
                        [ "name" .= Aeson.Null
                        , "email" .= ("bobbydee@hotmail.com" :: Text)
                        ]
                  ]
              , "Email: bobbydee@hotmail.com\n\nThis is another example message from a test."
              )
            ,
              ( Aeson.object
                  [ "message" .= ("This is another example message from a test." :: Text)
                  , "from"
                      .= Aeson.object
                        [ "name" .= ("Mr. Bob Dobalina" :: Text)
                        , "email" .= Aeson.Null
                        ]
                  ]
              , "Name: Mr. Bob Dobalina\n\nThis is another example message from a test."
              )
            ,
              ( Aeson.object
                  [ "message" .= ("This is another example message from a test." :: Text)
                  , "from"
                      .= Aeson.object
                        [ "name" .= ("Mr. Bob Dobalina" :: Text)
                        , "email" .= ("bobbydee@hotmail.com" :: Text)
                        ]
                  ]
              , "Name: Mr. Bob Dobalina\nEmail: bobbydee@hotmail.com\n\nThis is another example message from a test."
              )
            ]

      for_ examples $ \(example, expectedEmailBody) -> do
        let title = LazyChar8.unpack $ Aeson.encode example

        it title $ \(testApp, sentEmailsVar) -> do
          response <- post manager testApp example
          [email] <- EmailStubs.getAllSentEmails sentEmailsVar

          Http.responseStatus response `shouldBe` HttpTypes.status202
          Http.responseBody response `shouldBe` "\"Message sent.\""

          email.from `shouldBe` testApp.appData.fromAddress
          email.to `shouldBe` coerce (testApp.appData.adminEmails)
          email.cc `shouldBe` []
          email.bcc `shouldBe` []

          email.subject `shouldBe` Just "Message sent through Disco's Lab"
          email.body `shouldBe` expectedEmailBody
